module gitee.com/mimis/protoc-gen-rpcx

go 1.16

require (
	gitee.com/mimis/golang-tool v0.0.0-20220222054036-e9b9a897a689
	github.com/golang/protobuf v1.5.2
)
