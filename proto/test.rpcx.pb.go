// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/test.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

import (
	context "context"
	client "gitee.com/mimis/golang-tool/rpcx/client"
	service "gitee.com/mimis/golang-tool/rpcx/service"
	"time"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

var serverName string = "pack"

type PackClientInterface interface {
	Signin(context.Context, *SigninReq) (*SigninRes, error)
}

func NewPackClient(etcdAddrs []string, timeout time.Duration, etcdBasePath string) PackClientInterface {
	c := client.New(serverName, etcdAddrs, timeout, etcdBasePath)

	return &PackClient{
		c: c,
	}
}

type PackClient struct {
	c *client.ClientManager
}

func (c *PackClient) Signin(ctx context.Context,
	in *SigninReq) (*SigninRes, error) {
	out := new(SigninRes)
	err := c.c.Call(ctx, "Signin", in, out)
	return out, err
}

type PackServiceInterface interface {
	Signin(context.Context, *SigninReq, *SigninRes) error
}

func RegisterPackService(s *service.ServerManage, hdlr PackServiceInterface) error {
	return s.RegisterOneService(serverName, hdlr)
}

func NewPackServiceAndRun(listenAddr, exposeAddr string, etcdAddrs []string, handler PackServiceInterface, etcdBasePath string) (*service.ServerManage, error) {
	s, err := service.New(exposeAddr, etcdAddrs, etcdBasePath)
	if err != nil {
		return nil, err
	}

	err = RegisterPackService(s, handler)
	if err != nil {
		return nil, err
	}

	go func() {
		err = s.Run(listenAddr)
		if err != nil {
			panic(fmt.Errorf("listen(%v) error(%v)", listenAddr, err))
		}
	}()

	return s, nil
}
